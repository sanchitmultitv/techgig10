import { Component, Input, OnInit } from '@angular/core';
declare var $:any;

@Component({
  selector: 'app-pdfmodal',
  templateUrl: './pdfmodal.component.html',
  styleUrls: ['./pdfmodal.component.scss']
})
export class PdfmodalComponent implements OnInit {
  @Input() rName:any;
  constructor() { }

  ngOnInit(): void {
  
  //   $('#iframe_feedback').on("load", ()=> {
  //     console.log('iframe loaded'); //replace with code to hide loader
  // });
  }
  closeModal(){
    $("#pdf_modal").modal('hide');
    $("#win_modal").modal('hide');
  }
}
