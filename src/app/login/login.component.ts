import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { DataService } from '../services/data.service';
import { Eventid } from '../shared/eventid';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  event_id = Eventid.event_id;
  isVideoPlayed = false;
  menu_type;
  newName: any;
  totalEmail: string;
  constructor(private fb: FormBuilder, private auth: AuthService, private router: Router,private _fd: DataService) { }

  ngOnInit(): void {
    localStorage.setItem('tour_guide', 'start_guide');
    this.loginForm = this.fb.group({
      email: ['', Validators.required]
      // last_name: ['', Validators.required]
    });
  }
  skip(){
    this.router.navigate(['/lobby']);
  }
  submitLogin(){
     const user = {
       email: this.loginForm.value.email,
     event_id: this.event_id,
       role_id: 1,
     };
  
    // var isMobile = {
    //   Android: ()=> {
    //       return navigator.userAgent.match(/Android/i);
    //   },
    //   BlackBerry: ()=> {
    //       return navigator.userAgent.match(/BlackBerry/i);
    //   },
    //   iOS: ()=> {
    //       return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    //   },
    //   Opera: ()=> {
    //       return navigator.userAgent.match(/Opera Mini/i);
    //   },
    //   Windows: ()=> {
    //       return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
    //   },
    //   any: ()=> {
    //       return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    //   }
    // };
  
    // this.auth.loginMethod(user).subscribe((res: any) => {
    //   if (res.code === 1) {
    //     localStorage.setItem('menu_type', JSON.stringify(res.result.menu_type));
    //     if( isMobile.iOS() ){
    //       this.isVideoPlayed = false;
    //       this.router.navigateByUrl('/lobby');
    //     }
    //     if(res.result.outer_animation == true){
    //       let vid: any = document.getElementById('outerVideo');
    //       let sources = vid.getElementsByTagName('source');
    //       sources[0].src = res.result.outer_animation_url;
    //       vid.load();
    //       vid.play();
    //     }
    //     localStorage.setItem('virtual', JSON.stringify(res.result));
    //     this.isVideoPlayed = true;
    //   } else {
    //     this.isVideoPlayed = false;
    //     this.loginForm.reset();
    //   }
    // }, (err: any) => {
    //   this.isVideoPlayed = false;
    //   console.log('error', err)
    // });
     if (this.loginForm.invalid) {
      return
    } else{      
    console.log('logindata', this.loginForm.value);
   
      const formData = new FormData();
      
    // this.newName =  (this.loginForm.value.first_name).trim();
    // this.totalEmail = this.newName+ Math.floor(Math.random() * 1000000) +'@pds.com';

      formData.append('email', this.totalEmail);
      formData.append('password','others');
      formData.append('mobile','others');
      formData.append('category','others');
      formData.append('company','others');
      formData.append('name', this.loginForm.value.first_name + ' ' + this.loginForm.value.last_name);
     // formData.append('headquarter', this.signupForm.get('job_title').value);
      var isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
        
    };
   
    this.auth.loginMethod(this.loginForm.value.email).subscribe((res: any) => {
      if (res.code === 1) {
        // let emails = this.totalEmail.toLowerCase();
        // this._fd.signupRaise(emails,this.loginForm.value.first_name).subscribe((res:any) => {

        // });
        //   this._fd.getloginforhand(emails).subscribe((res: any) => {
        //     localStorage.setItem('getdata', JSON.stringify(res.result));
        //   })
        //   localStorage.setItem('myemail', emails);

        if( isMobile.iOS() ){
          this.isVideoPlayed = false;
          this.router.navigateByUrl('/lobby');
        }
        // this.isVideoPlayed = true;
        localStorage.setItem('virtual', JSON.stringify(res.result));
        // this.isVideoPlayed = true;
        // let vid: any = document.getElementById('outerVideo');
        // vid.play();
          this.router.navigateByUrl('/lobby');

        /* if (window.innerHeight>window.innerWidth){
          this.potrait = true;
        }else{
          this.potrait = false;
        } */
      } else {
      //  this.msg = 'Invalid Login';
        this.isVideoPlayed = false;
        this.loginForm.reset();
      }
        // if (res.code === 1) {
        //        localStorage.setItem('menu_type', JSON.stringify(res.result.menu_type));
        //        if( isMobile.iOS() ){
        //          this.isVideoPlayed = false;
        //          this.router.navigateByUrl('/lobby');
        //        }
        //        if(res.result.outer_animation == true){
        //          let vid: any = document.getElementById('outerVideo');
        //          let sources = vid.getElementsByTagName('source');
        //          sources[0].src = res.result.outer_animation_url;
        //         vid.load();
        //          vid.play();                
        //        }
        //        localStorage.setItem('virtual', JSON.stringify(res.result));
        //        this.isVideoPlayed = true;
        //     } else {
        //       this.isVideoPlayed = false;
        //      // this.loginForm.reset();
        //     }
          }, (err: any) => {
            this.isVideoPlayed = false;
            console.log('error', err)
           });
 // this.profileForm.reset();

  }}
  

  endVideo(){
    this.router.navigate(['/lobby']);
  }
}
