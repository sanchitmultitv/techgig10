import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SignaturewallRoutingModule } from './signaturewall-routing.module';
import { SignaturewallComponent } from './signaturewall.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [SignaturewallComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SignaturewallRoutingModule
  ]
})
export class SignaturewallModule { }
