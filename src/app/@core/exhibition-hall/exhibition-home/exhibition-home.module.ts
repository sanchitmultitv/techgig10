import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExhibitionHomeRoutingModule } from './exhibition-home-routing.module';
import { ExhibitionHomeComponent } from './exhibition-home.component';
import { PdfmodalModule } from 'src/app/@main/pdfmodal/pdfmodal.module';
@NgModule({
  declarations: [ExhibitionHomeComponent],
  imports: [
    CommonModule,
    ExhibitionHomeRoutingModule,PdfmodalModule
  ]
})
export class ExhibitionHomeModule { }
