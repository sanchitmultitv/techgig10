import { Component, HostListener, OnInit, Renderer2, ViewChild } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';
import { DataService } from 'src/app/services/data.service';
declare var $: any;
// const FileSaver = require('file-saver');
// import { FileSaver } from 'file-saver';
import { saveAs } from 'file-saver';
import * as JSZip from 'jszip';
import * as FileSaver from 'file-saver';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2';
import { ExhibitorChatComponent } from '../exhibitor-chat/exhibitor-chat.component';

@Component({
  selector: 'app-exhibition-stall',
  templateUrl: './exhibition-stall.component.html',
  styleUrls: ['./exhibition-stall.component.scss']
})
export class ExhibitionStallComponent implements OnInit {
  stallData = [];
  bgImg;
  open_popup = 'open_popup';
  receiver_name;
  fileName = 'sample.pdf';
  boothList = [
    // {name: 'play video', src:'assets/event/playvideo.png', opener:'open_popup'},
    { name: 'brochures', src: 'assets/event/brochure.png', opener: 'open_popup' },
    { name: 'enquiry form', src: 'assets/event/enquiryform.png', opener: 'enquiry' },
    { name: 'exhibitor chat', src: 'assets/event/exhibitor_chat.png', opener: 'exhibitor_chat' },
    // {name: 'briefcase', src:'assets/event/Briefcase.png', opener:'open_popup'},
    // { name: 'schedule call', src: 'assets/event/Briefcase.png', opener: 'open_popup' },
  ];
  modalTitle = 'Play Video';
  creatives = {};
  mainID: any;
  timeVal;
  maindata = [];
  loop: string;
  chat_id: any;
  chat_name: any;
  newTimeSlots = [];
  bsInlineValue = new Date();
  bsInlineRangeValue: Date[];
  maxDate = new Date();
  check: any;
  claimedDate: any;
  booth_id: any;

  @ViewChild(ExhibitorChatComponent) 'xyz': ExhibitorChatComponent;
  months: number;
  year: number;
  day: number;
  month: number;
  exhibitionName: string;

  constructor(private _ds: DataService, private _ar: ActivatedRoute, private http: HttpClient, private renderer: Renderer2) {
    this.maxDate.setDate(this.maxDate.getDate() + 7);
    this.bsInlineRangeValue = [this.bsInlineValue, this.maxDate];
  }

  ngOnInit(): void {
    document.getElementById('chatSection').style.transform = 'translateX(3000px)';
    this._ar.paramMap.subscribe((params: Params) => {
      this.getStallData(params.get('id'));
    });
    this.getExhibitionData();
    // this.onDateSelect();
  }
  getStallData(id) {
    this._ds.getStallData(id).pipe(map((data: any) => {
      this.stallData = data.result;
      // this.receiver_name = this.stallData[0]['title'];
      this.creatives = this.stallData[0]['creatives']
      console.log(this.creatives)
      // console.log(this.stallData)
    })).subscribe();
  }
  openBooth(item) {
    console.log(item)
    document.getElementById("ModalPlay").className = "modal-dialog modal-lg";
    if (item.opener == "exhibitor_chat") {
      // document.getElementById('exhibitor_chat').style.display="block";
      $('#call_exhibitor').modal('show');
      // exhibitor is starting from here 
      // document.getElementById('chatSection').style.transform="translate(-15px)";
      // document.getElementById('chatSection').style.transition=".5s";
      // exhibitor is ending here

    } else {
      $('#' + item.opener + '_modal').modal('show');
      if (item.name == 'play video') {
        // this.playVideo();
        this.creation();
      }
      if (item.name == 'brochures') {
        this.modalTitle = 'brochures';
      }
      if (item.name == 'briefcase') {
        this.modalTitle = 'briefcase';
      }
    }
  }
  closeModal() {
    $('#open_popup_modal').modal('hide');
    $('#call_exhibitor').modal('hide');
    // const exhibitPlayVideo:any = document.getElementById('exhibitPlayVideo');
    // if(this.open_popup=='open_popup'){
    //   exhibitPlayVideo.currentTime = 0;
    //   exhibitPlayVideo.pause();
    // }
    this.remove();
  }
  playVideo() {
    this.modalTitle = 'Play Video';
    const exhibitPlayVideo: any = document.getElementById('exhibitPlayVideo');
    let sources = exhibitPlayVideo.getElementsByTagName('source');
    sources[0].src = (this.stallData[0].video != '') ? this.stallData[0].video : 'https://d3ep09c8x21fmh.cloudfront.net/techgig10dec/speedhire.mp4';
    exhibitPlayVideo.load();
    exhibitPlayVideo.play();
    this.open_popup = 'open_popup';
  }
  pointerMethod(c, p) {
    // this.open_popup = 'open_popup_creative';
    this.modalTitle = c;
    $('#open_popup_modal').modal('show');
    // let embed1: any = document.getElementById('embed1');
    // embed1.src = p;
    document.getElementById("ModalPlay").className = "modal-dialog modal-md";
    var popupData: any = document.getElementById("popupData");
    var embed: any = document.createElement('EMBED');
    embed.id = 'embed1';
    embed.setAttribute("src", p);
    embed.setAttribute("width", "100%");
    embed.setAttribute("height", "540");
    embed.setAttribute("frameborder", "0");
    popupData.append(embed);
  }
  creation() {
    $('#open_popup_modal').modal('show');

    this.modalTitle = 'Play Video';
    var popupData: any = document.getElementById("popupData");
    var vid: any = document.createElement("VIDEO");
    vid.id = "playVideo";
    if (vid.canPlayType("video/mp4")) {
      vid.setAttribute("src", (this.stallData[0].video != '') ? this.stallData[0].video : 'https://d3ep09c8x21fmh.cloudfront.net/techgig10dec/speedhire.mp4');
    }
    vid.setAttribute("width", "100%");
    vid.setAttribute("height", "auto");
    vid.setAttribute("controls", "controls");
    vid.setAttribute("preload", "true");
    vid.setAttribute("autoplay", "true");
    vid.setAttribute("poster", "assets/event/lobby.jpg");
    popupData.append(vid);
  }
  newVideo() {

  }

  getExhibitionData() {
    this._ar.paramMap.subscribe((params: Params) => {
      this.mainID = params.get('id');
    });
    this._ds.getAllExhibition(this.mainID).subscribe((res: any) => {
      if (res.code == 1) {
        this.maindata = res.result;
        console.log(this.maindata, 'maindata')
      } else {
        console.log('data not found')
      }
    });
  }

  callExhibitionChat(id, name) {

    $('#call_exhibitor').modal('hide');

    this.chat_id = id;
    this.chat_name = name;
    this.xyz.newFunction(this.chat_id, this.chat_name);
    document.getElementById('chatSection').style.transform = "translate(-15px)";
    document.getElementById('chatSection').style.transition = ".5s";
  }

  openVideoCall(newedid, name) {
    console.log(newedid)
    this.booth_id = newedid;
    console.log(name)
    $('#call_exhibitor').modal('hide');
    $('#call_video_chat').modal('show');

  }
  // onDateSelect(){

  //   this._ds.totalTimeSlots(2151,767,2021-10-10).subscribe(res=>{
  //     console.log('timeresponse',res);
  //     this.newTimeSlots=res.result;
  //   });
  // }
  parseDate(dates) {
    console.log(dates)

    // console.log('aycycyt', new Date(dates).getFullYear());
    this.year = new Date(dates).getFullYear();
    this.months = new Date(dates).getMonth();
    this.month = this.months + 1;
    this.day = new Date(dates).getDate()
    // console.log('aycycyt', new Date(dates).getMonth());
    // console.log('aycycyt', new Date(dates).getDate());
    let myDate = this.year + '-' + this.month + '-' + this.day;
    this._ar.paramMap.subscribe(params => {
      this.exhibitionName = params.get('id');
      // let data = JSON.parse(localStorage.getItem('virtual'));
      this._ds.totalTimeSlots(this.booth_id, this.exhibitionName, myDate).subscribe(res => {
        console.log('timeresponse', res);
        this.newTimeSlots = res.result;
      })
    });

    console.log('aycycyt', this.year + '-' + this.month + '-' + this.day);

  }

  getTime(event: any, valClass, time) {
    this.timeVal = time;
    const hasClass = event.target.classList.contains(valClass);
    $(".time-list li a.active").removeClass("active");
    // adding classname 'active' to current click li
    this.renderer.addClass(event.target, valClass);
  }

  // confirm() {
  //   this._ar.paramMap.subscribe(params => {
  //     this.exhibitionName = params.get('id');
  //     let data = JSON.parse(localStorage.getItem('virtual'));
  //     console.log(this.model.year + '-' + this.model.month + '-' + this.model.day + ' ' + this.timeVal);
  //     const CallData = new FormData();
  //     CallData.append('exhibition_id', this.ExhibitionID);
  //     CallData.append('user_id', data.id);
  //     CallData.append('stall_id', this.exhibitionName)
  //     CallData.append('time', this.model.year + '-' + this.model.month + '-' + this.model.day + ' ' + this.timeVal);
  //     this._ds.scheduleCallGet(this.token, CallData).subscribe(res => {
  //       console.log(res);
  //       if (res.code == 1) {
  //         // this.toastr.success('Call scheduled succesfully!');

  //         let myDate = this.model.year + '-' + this.model.month + '-' + this.model.day;
  //         //let myDate = '2020-10-25';
  //         let data = JSON.parse(localStorage.getItem('virtual'));
  //         this._ds.totalTimeSlots(this.ExhibitionID, this.exhibitionName, myDate).subscribe(res => {
  //           console.log('timeresponse', res);
  //           this.newTimeSlots = res.result;
  //           //  alert(this.newTimeSlots)
  //           console.log(this.newTimeSlots)
  //         })
  //         setTimeout(() => {
  //           $('.scheduleCallmodal').modal('hide');
  //         }, 2000);
  //       }

  //     });

  //   });
  // }

  call_video_chatModal() {
    $('#call_video_chat').modal('hide');
  }

  remove() {
    var popupData: any = document.getElementById("popupData").childNodes;
    popupData.forEach(ele => {
      console.log(ele.id);
      document.getElementById(ele.id).remove();
    });
  }

  @HostListener('document:click', ['$event', '$event.target'])
  onClick(event: MouseEvent, targetElement: HTMLElement): void {
    let open_popup_modal: any = document.getElementById('open_popup_modal');
    if (targetElement === open_popup_modal) {
      open_popup_modal.style.display = "none";
      this.remove();
    }
  }
  downloadPdf(url) {
    let pdfUrl = url;
    let pdfName = 'sample.pdf';
    saveAs(pdfUrl, pdfName);
  }
  selectZip(event) {
    console.log(event.target.checked)
  }
  selectZipAll(count) {
    for (let i = 0; i < count; i++) {
      this['check' + i + 1] = document.getElementById('check_' + (i + 1));
      this['check' + i + 1].checked = !this['check' + i + 1].checked;
      // console.log(this['check'+i+1].checked, this['check'+i+1])
      this.createFile(this.stallData[0]['brochure'][i]['url']);
    }
  }
  async createFile(url) {
    let response = await fetch(url);
    let data = await response.blob();
    let metadata = {
      type: 'application/pdf'
    };
    let file = new Blob([data], metadata);
    console.log(file)
    this.pdfByteArrays.push(file);
  }
  convertBlobToBase64(blob: Blob) {
    // console.log('pppp', blob)  
    return Observable.create(observer => {
      const reader = new FileReader();
      const binaryString = reader.readAsDataURL(blob);
      reader.onload = (event: any) => {
        console.log('Image in Base64: ', event.target.result);
        observer.next(event.target.result);
        observer.complete();
      };
      reader.onerror = (event: any) => {
        console.log("File could not be read: " + event.target.error.code);
        observer.next(event.target.error.code);
        observer.complete();
      };
    });
  }
  zip = new JSZip();
  pdfByteArrays = [];


  downloadZip() {
    var zip = new JSZip();
    zip.file("Hello.txt", "Hello World\n");
    var pdf = zip.folder("pdfs");
    this.pdfByteArrays.forEach((value, i) => {
      pdf.file((i + 1) + '.pdf', value, { base64: true });
    });
    zip.generateAsync({ type: "blob" }).then(function (content) {
      FileSaver.saveAs(content, "example.zip");
    });
  }

  addToBriefcase(item) {
    const formData = new FormData();
    formData.append('user_id', JSON.parse(localStorage.getItem('virtual')).id);
    formData.append('document_url', item.url);
    formData.append('document_title', item.title);
    this._ds.addToBriefcaseList(formData).subscribe((res: any) => {
      if (res.code == 1) {
        Swal.fire({
          position: 'top',
          icon: 'success',
          title: 'Added to Briefcase!!',
          showConfirmButton: false,
          timer: 3000
        });
      }
      if (res.code == 0) {
        Swal.fire({
          position: 'top',
          icon: 'error',
          title: res.result,
          showConfirmButton: false,
          timer: 3000
        });
      }
    });
  }
}
