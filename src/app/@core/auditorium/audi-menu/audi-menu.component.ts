import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
declare var $:any;
import swal from 'sweetalert';
@Component({
  selector: 'app-audi-menu',
  templateUrl: './audi-menu.component.html',
  styleUrls: ['./audi-menu.component.scss']
})
export class AudiMenuComponent implements OnInit {
  @Output() myOutput:EventEmitter<any> = new EventEmitter();
  Leftmenu=[
    {name:'clap', img:'assets/icons/clap.png', audio:'assets/mp3/applause.mp3'},
    {name:'like', img:'assets/icons/like.png', audio:'like'},
    {name:'heart', img:'assets/icons/heart.png', audio:'heart'},
  ];
  Rightmenu=[
    {name:'ask question', img:'assets/icons/asks.png'},
    // {name:'quiz', img:'assets/icons/quiz.png', audio:null},
    // {name:'poll', img:'assets/icons/poll.png'},
    // {name:'group chat', img:'assets/icons/chat-h.png'},
    // {name:'hand raise', img:'https://webinar.multitvsolution.com/handraise/images/raise-hand.png'}
  ];
  flag: any = 0;
  imag:any;
  like = false;

  constructor(private _ds: DataService) { }

  ngOnInit(): void {
  }

  openRigthMenu(item){
    if(item.includes('question')){
      $("#ask_question_modal").modal("show");
    }
    if(item == 'poll'){
      $("#poll_modal").modal("show");
    }
    if(item == 'group chat'){
      $("#t_one_groupChat").modal("show");
    }
    if(item == 'hand raise'){
     this.toggleHandRaise();
    }
    if(item == 'quiz'){
      $("#quiz_modal").modal("show");
     }
  }

  likeopen(data){
    if(data =='like'){
      this.imag = 'assets/icons/like.png'
    }
    if(data =='assets/mp3/applause.mp3'){
      this.imag = 'assets/icons/clap.png'
    }
    if(data =='heart'){
      this.imag = 'assets/icons/heart.png'
    }
    this.like = true;
    setTimeout(() => {
      this.like = false;
    }, 10000);
  }

  toggleHandRaise() {
    swal("Success", "Your request has been sent", "success");
    if (this.flag == 0) {
      this.flag = 1;

    }
    else {
      this.flag = 0;

    }
    this.handraise();
  }
  handraise() {
    let signify_userid = JSON.parse(localStorage.getItem('getdata')).id;
    const formsData = new FormData();
    formsData.append('user_id', signify_userid);
    formsData.append('flag', this.flag);
    console.log(formsData);
    this._ds.handraise(formsData).subscribe(res => {
      console.log(res);
    })

  }

  openLeftMenu(item){
    if(item == null) {
      $("#quiz_modal").modal("show");
    } else {
      var sound:any = document.createElement('audio');
      sound.id = 'audio-player';
      sound.src = item
      sound.type = 'audio/mpeg';
      sound.preload = true;
      sound.autoplay = '';
      sound.controls = 'controls';
      sound.play();
      sound.style.display='none';
      document.getElementById('setAudio').appendChild(sound);
    }
  }
}
