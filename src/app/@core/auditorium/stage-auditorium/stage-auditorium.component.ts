import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { DataService, localService } from 'src/app/services/data.service';
declare var Clappr:any;
declare var $:any;

@Component({
  selector: 'app-stage-auditorium',
  templateUrl: './stage-auditorium.component.html',
  styleUrls: ['./stage-auditorium.component.scss']
})
export class StageAuditoriumComponent implements OnInit, AfterViewInit, OnDestroy {
  stream_url;
  audiObj:any = {};
  poster:any;
  CPlayer;
  myImgUrl = '/assets/poster.jpg';
  auditorium_id;
  constructor(private _ds:DataService, private _ar:ActivatedRoute, private _ls: localService) { }

  ngOnInit(): void {
    this.getActiveAuditorium();
  }
  ngAfterViewInit(){
    this.beatAnalytics();
  }
  playStream(stream,poster) {
    var playerElement = document.getElementById("player-wrapper");
    this.CPlayer = new Clappr.Player({
      source: stream,
      // source: 'https://d17uqpjc0q0ra5.cloudfront.net/abr/smil:session10-p.smil/playlist.m3u8',
      poster: poster,
      // mute: false,
      autoPlay: true,
      height: window.innerHeight/this.audiObj.player_height,
      width: window.innerWidth/this.audiObj.player_width,
      playInline: true,
      hideMediaControl: false,
      // hideSeekBar: true,
      // visibilityEnableIcon: false,

    });
    
    this.CPlayer.attachTo(playerElement);
    // $('#player-wrapper > div > .media-control').css({ 'height': '1'});
    
  }
  getActiveAuditorium(){
    this._ar.paramMap.subscribe(params => {
      const id = params.get('id');
      this.auditorium_id = id;
      this._ds.getActiveAuditorium(id).subscribe((res:any)=>{
        console.log('res',res);
        this.audiObj = res.result?.[0];
        this.poster = res.result?.[0].poster;
        this.playStream(res.result?.[0].stream,this.poster);
      });
    });
  }
  clearInterval;
  beatAnalytics() {
    this._ls.stepUpAnalytics('auditorium_'+this.auditorium_id);
    this._ls.getHeartbeat(this.auditorium_id);
    this.clearInterval = setInterval(() => {
      this._ls.getHeartbeat(this.auditorium_id);
    }, 60000);    
  }
  ngOnDestroy(){
    clearInterval(this.clearInterval);
  }
}
