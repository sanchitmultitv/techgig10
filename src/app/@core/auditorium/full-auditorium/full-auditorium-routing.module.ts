import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FullAuditoriumComponent } from './full-auditorium.component';

const routes: Routes = [
  {path:'', component:FullAuditoriumComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FullAuditoriumRoutingModule { }
