import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { DataService, localService } from 'src/app/services/data.service';
declare var Clappr:any;
declare var $:any;
@Component({
  selector: 'app-full-auditorium',
  templateUrl: './full-auditorium.component.html',
  styleUrls: ['./full-auditorium.component.scss']
})
export class FullAuditoriumComponent implements OnInit, AfterViewInit { 
  audi_id;
  // stream_url = 'https://wowzaec2demo.streamlock.net/vod-multitrack/_definst_/smil:ElephantsDream/ElephantsDream.smil/playlist.m3u8';
  stream_url='assets/Auditorium.mp4';
  CPlayer:any;
  audi_obj = {};
  poster: any;
  constructor(private _ds: DataService, private _ar: ActivatedRoute, private _ls:localService) { }

  ngOnInit(): void {
    this.getActiveAuditorium();
    this.beatAnalytics();
  }
  ngAfterViewInit(){
  }
  
  getActiveAuditorium(){
    this._ar.paramMap.subscribe(params => {
      this.audi_id = params.get('id');
      this._ds.getActiveAuditorium(this.audi_id).subscribe((res:any)=>{
        // console.log('res',res);
        // alert(res.result[0].poster)
        this.audi_obj = res.result?.[0];
        // this.poster=res.result?[0].poster;
        this.playStream(res.result?.[0].stream,res.result[0].poster)
        // alert(this.poster)
        
      });
    });
  }
  beatAnalytics() {
    this._ar.paramMap.subscribe((params:any) => {
      this._ls.stepUpAnalytics('auditorium_'+params.params['id']);
      this._ls.getHeartbeat(params.params['id']);
      this._ds.getActiveAuditorium(params.params['id']).pipe(map(res=>{
        // console.log('res',res);
      })).subscribe();
    });    
  }
  playStream(stream,poster) {
    var playerElement = document.getElementById("player-wrapper");
    this.CPlayer = new Clappr.Player({
      source: stream,
      // source: 'https://d17uqpjc0q0ra5.cloudfront.net/abr/smil:session10-p.smil/playlist.m3u8',
      poster: poster,
      // mute: false,
      autoPlay: true,
      height: '100vh',
      width: '100%',
      playInline: true,
      hideMediaControl: false,
      // hideSeekBar: true,
      // visibilityEnableIcon: false,

    });
    
    this.CPlayer.attachTo(playerElement);
    // $('#player-wrapper > div > .media-control').css({ 'height': '1'});
    
  }
 
}
