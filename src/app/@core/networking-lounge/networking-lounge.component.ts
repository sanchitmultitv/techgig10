import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { DataService } from 'src/app/services/data.service';
import { ExhibitorChatComponent } from '../exhibition-hall/exhibitor-chat/exhibitor-chat.component';
declare var $:any;
@Component({
  selector: 'app-networking-lounge',
  templateUrl: './networking-lounge.component.html',
  styleUrls: ['./networking-lounge.component.scss']
})
export class NetworkingLoungeComponent implements OnInit {
  bgImg;
  pointers=[];
  sName='agenda';
  chat_name;
  chat_id;
  attendeesList = [];
  user_name = JSON.parse(localStorage.getItem('virtual')).user_name;
  @ViewChild(ExhibitorChatComponent) chatcomp: ExhibitorChatComponent;
  @Output() newItemEvent = new EventEmitter<any>();

  constructor(private _ds:DataService, private router: Router) { }

  ngOnInit(): void {
    this._ds.getSettingSection().subscribe(res=>{
      this.bgImg = res['bgImages']['lounge'];
      this.pointers = res['networkinLoungePointers'];
    });
    this.getAllAttendees();
  }
  getAllAttendees(){
    // this._ds.getAllAttendees().subscribe(res=>{
    //   this.attendeesList = res.result; 
    // })
    // this._ds.getExhibitionFullHallsData().pipe(map((data:any)=>{
    //   this.attendeesList = data.result[0]['stall_list'][0]['stalls'];
    //   console.log(data.result[0]['stall_list'])
    //   console.log(this.attendeesList)
    // })).subscribe();
    let mainID = 762;
    
    this._ds.getAllExhibition(mainID).subscribe((res: any) => {
      if (res.code == 1) {
        this.attendeesList = res.result;
        console.log(this.attendeesList, 'maindata')
      } else {
        console.log('data not found')
      }

    });

  }
  pointerMethod(item){
    $(`.${item.class}`).tooltip('hide');
    if(item.path != null){
      this.router.navigate([item.path]);
    }else {
      if(item.title.includes('chat')){
        this.openChatSection();
      }else{
        $('#pdf_modal').modal('show');
      }
    }
  }

  openChatSection(){
    const textbox:any = document.getElementById('a_chat_listing');
    textbox.style.transform = 'translate(-15px)';
    textbox.style.transition ='.5s'; 
  }
  gotoChatroom(list){
    console.log(list)
    // alert("halt")
    this.chat_id = list.id;
    this.chat_name = list.name;
  
    this.chatcomp.enterChatlist2(this.chat_id,list.id);
    this.closeChatListBox();
    document.getElementById('chatSection').style.transform='translateX(-15px)';
  }
  closeChatListBox(){
    const textbox:any = document.getElementById('a_chat_listing');
    textbox.style.transform = 'translateY(3000px)';
    textbox.style.transition ='.5s';    
  }
}
